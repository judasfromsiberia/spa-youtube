import Vue from "vue";
import VueRouter from "vue-router";
import store from "@/store/index.js";
import Login from "../views/Login.vue";
import Search from "../views/Search.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/login",
    name: "Login",
    component: Login,
    meta: {
      notAuth: true
    }
  },
  {
    path: "/search",
    name: "Search",
    component: Search,
    props: route => ({
      ...route.params
    }),
    meta: {
      requiresAuth: true
    }
  },
  {
    path: "/favorites",
    name: "Favorites",
    component: () => import("@/views/Favorites.vue"),
    meta: {
      requiresAuth: true
    }
  },
  {
    path: "/*",
    redirect: "/login"
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});
router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (store.getters.isLoggedIn) {
      next();
      return;
    }
    next("/login");
  } else {
    next();
  }
  if (to.matched.some(record => record.meta.notAuth)) {
    if (!store.getters.isLoggedIn) {
      next();
      return;
    }
    next("/search");
  } else {
    next();
  }
});
export default router;
