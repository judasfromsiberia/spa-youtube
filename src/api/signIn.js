import logins from "@/assets/logins.json";
function signIn(data) {
  const nameIndex = logins.findIndex(elem => elem.login == data.login);
  if (nameIndex >= 0) {
    if (logins[nameIndex].password == data.password) {
      return "token " + data.login;
    }
  }
  return false;
}
export { signIn };
