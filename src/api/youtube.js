import axios from "axios";
const youtube = axios.create({
  baseURL: "https://www.googleapis.com/youtube/v3/search",
  headers: {
    "content-type": "multipart/form-data"
  }
});

export default youtube;
