import youtube from "@/api/youtube.js";

async function searchVideos(data) {
  return youtube
    .get("", {
      params: {
        part: "snippet",
        q: data.q,
        maxResults: data.maxResult,
        order: data.order,
        key: "AIzaSyCVdTFfoi1R7gTyWMMoHJWAiNIe9sdNQW4"
      }
    })
    .then(function(response) {
      //handle success
      return response;
    })
    .catch(function(error) {
      //handle error
      console.log(error);
      console.log(error.response.data);
      console.log(error.response.status);
      console.log(error.response.headers);
    });
}
export { searchVideos };
