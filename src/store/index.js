import Vue from "vue";
import Vuex from "vuex";
import { signIn } from "@/api/signIn.js";
import { searchVideos } from "@/api/searchVideos.js";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    token: localStorage.getItem("token") || ""
  },
  getters: {
    isLoggedIn: state => !!state.token
  },
  mutations: {
    updateToken(state, data) {
      state.token = data.token;
    }
  },
  actions: {
    async signIn(ctx, data) {
      const response = await signIn(data);
      if (response) {
        localStorage.setItem("token", response);
        ctx.commit("updateToken", {
          token: response
        });
        return true;
      } else {
        return false;
      }
    },
    async searchVideos(ctx, data) {
      const response = await searchVideos(data);
      return response;
    },
    async logOut(ctx) {
      localStorage.removeItem("token");
      ctx.commit("updateToken", {
        token: ""
      });
    },
    async addFav(ctx, data) {
      let favs = JSON.parse(localStorage.getItem("fav" + ctx.state.token));
      if (favs) {
        favs.push(data);
        localStorage.clear("fav" + ctx.state.token);
        localStorage.setItem("fav" + ctx.state.token, JSON.stringify(favs));
      } else {
        const list = [data];
        localStorage.setItem("fav" + ctx.state.token, JSON.stringify(list));
      }
    },
    async editFav(ctx, data) {
      let favs = JSON.parse(localStorage.getItem("fav" + ctx.state.token));
      if (favs) {
        favs[data.index] = data.editedFav;
        localStorage.clear("fav" + ctx.state.token);
        localStorage.setItem("fav" + ctx.state.token, JSON.stringify(favs));
      }
    },
    async getFavs(ctx) {
      const token = (await "fav") + ctx.state.token;
      let favs = await JSON.parse(localStorage.getItem(token));
      if (favs) {
        return favs;
      } else {
        return [];
      }
    }
  },
  modules: {}
});
